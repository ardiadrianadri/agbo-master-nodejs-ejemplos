var express = require('express');
var router = express.Router();


// Auth con HTTP Basic Auth
//var basicAuth = require('../../lib/basicAuth');
//router.use(basicAuth('user', 'pass'));

// Auth con ApiKey
//var apiKeyAuth = require('../../lib/apiKeyAuth');
//router.use(apiKeyAuth);

// Auth con JWT
var jwtAuth = require('../../lib/jwtAuth');
router.use(jwtAuth());


/* GET home page. */
router.get('/', function(req, res) {
    res.json({
        ok: true,
        message: 'Zona de admin con usuario verificado.!'
    });
});

module.exports = router;
