"use strict";

// abrir fichero package.json

var fs = require('fs');

function leeDependencies( cb) {

    fs.readFile('./package.json', function (err, data) {
        if (err) {
            return cb(err);
        }

        try {

            // parsearlo
            var packageJson = JSON.parse(data);

        } catch (e) {

            return cb(e);

        }

        // devolver las dependencias

        return cb(null, Object.getOwnPropertyNames(packageJson.dependencies));

    });




}

module.exports = leeDependencies;