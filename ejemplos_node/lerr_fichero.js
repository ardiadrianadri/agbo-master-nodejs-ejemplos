'use strict';

var fs = require('fs');
var path = require('path');

var fichero = path.join('./', 'pruebas.js');

console.log('Abrir ' + fichero);

// Abrir el fichero
fs.readFile(fichero, {encoding: 'utf8'},function(err, data) {
    if (err) {
        console.log(err);
        return;
    }

    //console.log(data);

	// convertir su contenido (JSON) en objeto
    var objeto = JSON.parse(data);

	// usamos el objeto
    console.log(objeto.nombre);

});
