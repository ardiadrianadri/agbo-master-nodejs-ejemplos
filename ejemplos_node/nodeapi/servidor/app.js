var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');


var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


app.use(express.static(   path.join(__dirname, 'public')     ));

// podemos poner más carpetas de estáticos
app.use(express.static(   path.join(__dirname, 'publicos2')     ));

var dbMysql = require('./lib/dbMysql');

var dbMongo = require('./lib/dbMongo'); // no es necesario asignarlo a nada
require('./models/Agente.js'); // no es necesario asignarlo a nada

app.use('/', require('./routes/index'));
app.use('/users', require('./routes/users'));
app.use('/mysql', require('./routes/mysql'));
app.use('/agentes', require('./routes/agentes'));
app.use('/apiv1/admin', require('./routes/apiv1/admin'));
app.use('/apiv1', require('./routes/apiv1/authenticate'));

// API Version 1

app.use('/apiv1/agentes', require('./routes/apiv1/agentes'));


// API Version 2

//app.use('/apiv2/agentes', require('./routes/apiv2/agentes'));


// añadimos nuevo controlador para /dependencies
app.use('/dependencies', require('./routes/dependencies'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
