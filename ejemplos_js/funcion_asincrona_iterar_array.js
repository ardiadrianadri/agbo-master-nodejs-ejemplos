"use strict";

console.log('empiezo');

// función con resultado asíncrono
// que no lo devuelve con return, si no en el callback
var escribeTras2Segundos = function(texto, callBack) {
    setTimeout( function(){
        console.log(texto);
        callBack();
    }, 1000); // reducido a 1 segundo para probar mejor
};

// función ayudante,
// que va a hacer llamadas a escribeTras2Segundos (func),
// con cada elemento del array que recibe (arr)
// cuando acabe llamará a callbackFin
function serie(arr, func, callbackFin) {
    if (arr.length > 0) {
        // saco el primer elemento del array y
        // llamo a escribeTras2Segundos con el elemento
        func(arr.shift(), function () {
            // cuando termine func, vuelvo a
            // llamarme a mismo (serie) para procesar el siguiente
            serie(arr, func, callbackFin);
        });
    } else {
        // si n llega a 0 es que he acabado,
        // llamo a la función que pasaron
        // para ello, callbackFin
        callbackFin();
    }
}

// Procesa el array que te paso con la función que te digo
// y cuando termines, haces lo que te digo en el callback.
serie(['one','two','three','four'], escribeTras2Segundos, function(){
    console.log('he terminado');
});
