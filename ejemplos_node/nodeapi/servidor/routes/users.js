var express = require('express');
var router = express.Router();

/* GET users listing. */
//router.get('/', function(req, res, next) {
//  res.send('respond with a resource');
//});


// recibiendo parámetros de forma opcional
router.get('/:id?', function(req, res, next) {

  // leemos lo que nos pasan en parámetros de ruta (/users/5)
  console.log('params:', req.params);

  var coches = req.params.id;

  // leemos lo que nos pasan en query string (/users?edad=43&genero=masculino
  console.log('query:', req.query);

  var edad = req.query.edad;


  res.send('respond with a resource');
});

// peticion post con body (usamos alguna utilidad como plugin de chrome postman)
router.post('/:id', function(req, res) {

  // vemos lo que nos pasan en el body
  console.log('body:', req.body);

  res.send('cogido el body!');
});


module.exports = router;
