'use strict';

// hacer una función que devuelve otra
// la segunda tendrá acceso al scope de su madre

function creaClosure(name) {
	var apel = 'S';
    return function(){

        console.log(name, apel);
    }
}

// creamos closures

var closure = creaClosure('tesla');

var closure2 = creaClosure('tesla2');

// usamos closures

closure();

closure2();