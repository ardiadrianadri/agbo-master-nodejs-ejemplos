//"use strict";

// definimos un objeto (notacion literal)
var luis = {
	name: 'Luis',
	iniciales: function(n, mayusculas) {
		var txt = this.name.substr(0, n);
	    console.log(mayusculas ? txt.toUpperCase() : txt);
	}
};

var programa = {
	name: 'Excel',
}; 

luis.iniciales(1); // L (this es persona)

luis.iniciales.call(programa, 2, false); // E - llamamos a persona.iniciales() con otro this 

luis.iniciales.apply(programa, [3, true]); // E - llamamos a persona.iniciales() con otro this 
