"use strict";

var express = require('express');

// crear una app de express

var app = express();

// le vamos a decir que escuche peticiones en /

app.get('/', function(req, res){
    console.log('peticion');
    // vamos a responder las peticiones
    res.send('Hello');
});

// arrancamos en puerto 3000
var server = app.listen(3000, function(){
    var port = server.address().port;
    console.log('servidor express arrancado en el puerto ' + port);
});
