var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  // añadimos más datos a la vista para que pueda representarlos
  res.render('index', {
    title: 'NodeApi',

    prueba: '<h1>esto es titulo</h1>',

    // array para que la vista lo itere
    lista: ['uno', 'dos', 'tres'],

    // par va a tener la paridad del segundo actual
    par: (new Date()).getSeconds() % 2 === 0  ? 'par' : 'impar'
  });

});

// añado otro método al controlador index,
// atenderá la petición GET /list
router.get('/list', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


module.exports = router;
