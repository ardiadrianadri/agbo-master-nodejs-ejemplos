"use strict";

var mongoose = require('mongoose');

// definir esquema de agente

var agenteSchema = mongoose.Schema({
    name: String,
    age: Number
});

// metodo estático que devuelve una lista de la BD
agenteSchema.statics.lista = function( criterios, callback) {

    // uso .find sin callback para que me de un objeto query sin ejecutar
    var query = Agente.find(criterios);

    query.sort('name');

    query.exec( function(err, rows) {
        if (err) {
            return callback(err);
        }

        return callback(null, rows);

    });
};


// metodo de instancia que incrementa edad a un agente
agenteSchema.methods.sumaEdad = function(cuanto) {
    this.age = this.age + cuanto;
    return this;
};



// exportar

var Agente = mongoose.model('Agente', agenteSchema);

module.exports = Agente;