"use strict";

// cargo librería http (librería del api de node) 
var http = require('http');

// creo un objeto servidor y le configuro un handler 
// para todos los tipos de peticiones
var server = http.createServer( function(request, response){
    response.writeHead(200, {'Content-type': 'text/html'});
    response.end('Wake up, Neo...');
});

// arranco el servidor aceptando conexiones en el puerto 1337
// https://nodejs.org/api/http.html#http_server_listen_port_hostname_backlog_callback
server.listen(1337, '127.0.0.1');

console.log('Servidor arrancado en http://127.0.0.1:1337');


